# Promo Code Validator

Welcome to Promo Code Validator, this is a Symfony project made for a technical test for Ekwateur

## What is it's purpose ?

The purpose of this project is to test if a particular promo code is valid and is currently used in an offer.

## How to use it ?

First, you will have to launch your symfony server with this command :
```
symfony server:start
```

Then you have two possibilities :

### Command line 

You can test your Promo Code using the symfony command line using this command :
```
php bin/console promo-code:validate MON_CODE_PROMO_A_TESTER
```
If everything is fine and your Promo Code works, this command will create a json document in the folder 'public/files' named 'MON_CODE_PROMO_A_TEST_informations.json' and it will contain these informations :

```json
{
   "promoCode":"ELEC_IS_THE_NEW_GAS",
   "endDate":"2022-04-13",
   "discountValue":3.5,
   "compatibleOfferList":[
      {
         "name":"EKWAE2000",
         "type":"ELECTRICITY"
      },
      {
         "name":"EKWAE3000",
         "type":"ELECTRICITY"
      }
   ]
}
```

If something went wrong, an error message will be displayed in your terminal.

### API call 

You can test your Promo Code using an API Call to this route : 
```
/promocode/validate/{promo_code_name} - GET
```

This route will return a json object : 

```
[
   true,
   {
      "promoCode":"ELEC_IS_THE_NEW_GAS",
      "endDate":"2022-04-13",
      "discountValue":3.5,
      "compatibleOfferList":[
         {
            "name":"EKWAE2000",
            "type":"ELECTRICITY"
         },
         {
            "name":"EKWAE3000",
            "type":"ELECTRICITY"
         }
      ]
   }
]
```

### Unit Testing

In order to launch the unit tests, use this command : './vendor/bin/phpunit'

## Conclusion

Thank you for using Promo Code Validator.

Feedback are always welcome.
