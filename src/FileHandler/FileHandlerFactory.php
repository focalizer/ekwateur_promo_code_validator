<?php
 
namespace App\FileHandler;

use App\Utils\Constant;
use App\Utils\ErrorCode;
use App\FileHandler\JSONFileHandler;

/**
 * FileHandlerFactory
 * 
 * FileHandlerFactory create a handler for the given file type
 */
class FileHandlerFactory
{    
    /**
     * create a file handler with the file type passed
     *
     * @param  string $file_type
     * @param  string $filename
     * @return Object
     */
    public static function create($file_type, $filename): Object
    {
        $file_handler = null;

        switch ($file_type) {
            case Constant::JSON:
                $file_handler = new JSONFileHandler($filename);
                break;
            default:
                throw new \Exception(ErrorCode::FILE_NOT_SUPPORTED);
        }

        return $file_handler;
    }
}