<?php
 
namespace App\FileHandler;
 
use App\Utils\Constant;
use App\Utils\ErrorCode;

/**
 * JSONFileHandler
 * 
 * JSONFileHandler handle Json file
 */
class JSONFileHandler
{
    private $filename;
    
    /**
     * __construct
     *
     * @param  string $filename
     * @return void
     */
    function __construct($filename)
    {
        $this->__set("filename", $filename);
    }
    
    /**
     * write in the file
     *
     * @param string $content
     * @return void
     */
    public function write($content): void
    {
        json_decode($content);
        if (json_last_error() !== JSON_ERROR_NONE) {
            throw new \Exception(ErrorCode::FILE_NOT_SUPPORTED);
        }

        if (!file_put_contents(Constant::DOWNLOAD_PATH . $this->filename . ".json", $content)) {
            throw new \Exception(ErrorCode::FILE_NOT_SUPPORTED);
        }
    }
    
    /**
     * get property value
     *
     * @param string $property
     * @return mixed
     */
    public function __get($property): mixed
    {
        if (!property_exists(self::class, $property)) {
            throw new \Exception(ErrorCode::UNDEFINED_PROPERTY);
        }

        return $this->$property;
    }
    
    /**
     * set property value
     *
     * @param  string $property
     * @param  mixed $value
     * @return void
     */
    public function __set($property, $value): void
    {
        if (!property_exists(self::class, $property)) {
            throw new \Exception(ErrorCode::UNDEFINED_PROPERTY);
        }

        $this->$property = $value;
    }
}