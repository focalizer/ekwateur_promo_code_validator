<?php

namespace App\Command;

use App\Service\PromoCodeService;
use App\FileHandler\FileHandlerFactory;
use Symfony\Component\HttpClient\CurlHttpClient;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * PromoCodeValidateCommand
 * 
 * PromoCodeValidateCommand is a command that validate a Promo Code
 * if there is an error with the promo code, an error is displayed.
 *
 * Otherwise, the informations related to the Promo Code is stored 
 * in public/files/PROMO_CODE_NAME_informations.json
 */
#[AsCommand(
    name: 'promo-code:validate',
    description: 'Add a short description for your command',
)]
class PromoCodeValidateCommand extends Command
{    
    /**
     * Command configuration
     *
     * @return void
     */
    protected function configure(): void
    {
        $this->addArgument('promo_code_name', InputArgument::REQUIRED, 'The promo code to validate');
    }

    /**
     * Command execution
     *
     * @param  InputInterface $input
     * @param  OutputInterface $output
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $promo_code_name = $input->getArgument('promo_code_name');

        $promo_code_service = new PromoCodeService();
        $promo_code_result = $promo_code_service->validate($promo_code_name, new CurlHttpClient(), new CurlHttpClient());

        if (!$promo_code_result[0]) {
            echo $promo_code_result[1];
            return Command::FAILURE;
        }

        $json_handler = FileHandlerFactory::create("json", $promo_code_name . "_informations");
        $json_handler->write(json_encode($promo_code_result[1]));

        return Command::SUCCESS;
    }
}
