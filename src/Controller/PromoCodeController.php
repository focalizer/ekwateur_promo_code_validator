<?php

namespace App\Controller;

use App\Service\PromoCodeService;
use Symfony\Component\HttpClient\CurlHttpClient;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * PromoCodeController
 * 
 * The PromoCodeController is a list of route that let you manage PromoCodes
 * 
 * Routes list :
 * - /promocode/validate/{promo_code_name} - GET
 */
class PromoCodeController extends AbstractController
{
    /**
     * Route to validate a given Promo Code
     *
     * @param  string $promo_code_name
     * @return Response
     */
    #[Route('/promocode/validate/{promo_code_name}', methods: ['GET'], name: 'promo_code_validate')]
    public function validate(string $promo_code_name): Response
    {
        $promo_code_service = new PromoCodeService();
        $validate_result = $promo_code_service->validate($promo_code_name, new CurlHttpClient(), new CurlHttpClient());
        return $this->json($validate_result);
    }
}
