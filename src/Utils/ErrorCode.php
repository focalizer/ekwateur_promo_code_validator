<?php
 
namespace App\Utils;
 
/**
 * ErrorCode
 * 
 * ErrorCode constants
 */
class ErrorCode
{
    const PROMO_CODE_DOESNT_EXIST = "This Promo Code doesn't exist";
    const PROMO_CODE_NOT_IN_OFFER = "This Promo Code has no offer associated";
    const PROMO_CODE_HAS_EXPIRED = "This Promo Code has expired";
    const API_ERROR = "An error occured";
    const FILE_NOT_SUPPORTED = "File type not supported";
    const UNDEFINED_PROPERTY = "This property is undefined in this class";
}