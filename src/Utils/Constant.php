<?php
 
namespace App\Utils;

/**
 * Constant
 */
class Constant
{
    const JSON = "json";
    const DOWNLOAD_PATH = "./public/files/";
}