<?php
 
namespace App\Service;

use Symfony\Contracts\HttpClient\HttpClientInterface;
use App\Service\EkwatestApiService;
use App\Utils\ErrorCode;
 
/**
 * PromoCodeService
 * 
 * This PromoCodeService offers helpful functionnalities for Promo Code like :
 * - Validate a Promo Code
 */
class PromoCodeService
{     
    /**
     * Validate a Promo Code
     * If the code is not valid, return the error.
     * Otherwise, it returns the code detail
     *
     * @param string $promo_code_name : Promo Code Name
     * @param HttpClientInterface $client_code_list : Code List client
     * @param HttpClientInterface $client_offer_list : Offer List client
     * @return array
     */
    public function validate($promo_code_name, HttpClientInterface $client_code_list, HttpClientInterface $client_offer_list): array
    {
        $ekwatest_api_service_code_list = new EkwatestApiService($client_code_list);
        $ekwatest_api_service_offer_list = new EkwatestApiService($client_offer_list);
        $promo_code_list = $ekwatest_api_service_code_list->getPromoCodeList();

        if (!$promo_code_list["success"]) {
            throw new \Exception(ErrorCode::API_ERROR);
        }

        for ($i = 0; $i < count($promo_code_list["response"]); ++$i) {
            if ($promo_code_list["response"][$i]["code"] != $promo_code_name) {
                continue;
            }

            $timestamp_expiration_date = strtotime($promo_code_list["response"][$i]["endDate"]);
            if ($timestamp_expiration_date < time()) {
                return [ false, ErrorCode::PROMO_CODE_HAS_EXPIRED ];
            }
            $compatible_offers = $this->getOffers($promo_code_name, $ekwatest_api_service_offer_list);
            if (!$compatible_offers[0]) {
                return $compatible_offers;
            }
            return [ true, $this->formatInformations($promo_code_list["response"][$i], $compatible_offers[1])];
        }

        return [ false, ErrorCode::PROMO_CODE_DOESNT_EXIST ];
    }
    
    /**
     * Return offers associated to a Promo Code
     * If no offers is assiociated, return the error
     *
     * @param string $promo_code_name
     * @param EkwatestApiService $ekwatest_api_service
     * @return array
     */
    private function getOffers($promo_code_name, EkwatestApiService $ekwatest_api_service): array
    {
        $offer_list = $ekwatest_api_service->getOfferList();

        if (!$offer_list["success"] ) {
            throw new \Exception(ErrorCode::API_ERROR);
        }

        $compatible_offers = [];
        for ($i = 0; $i < count($offer_list["response"]); ++$i) {
            if (!in_array($promo_code_name, $offer_list["response"][$i]["validPromoCodeList"])) {
                continue;
            }
            $compatible_offers[] = $offer_list["response"][$i];
        }

        if (!empty($compatible_offers)) {
            return [ true, $compatible_offers ];
        }
        return [ false, ErrorCode::PROMO_CODE_NOT_IN_OFFER ];
    }
    
    /**
     * Format informations of a promo code
     *
     * @param array $promo_code
     * @param array $compatible_offers
     * @return array
     */
    private function formatInformations($promo_code, $compatible_offers): array
    {
        $promo_code_informations = [
            "promoCode" => $promo_code["code"],
            "endDate" => $promo_code["endDate"],
            "discountValue" => $promo_code["discountValue"],
            "compatibleOfferList" => [],
        ];

        for ($i = 0; $i < count($compatible_offers); ++$i) {
            $promo_code_informations["compatibleOfferList"][] = [
                "name" => $compatible_offers[$i]["offerName"],
                "type" => $compatible_offers[$i]["offerType"]
            ];
        }

        return $promo_code_informations;
    }
}