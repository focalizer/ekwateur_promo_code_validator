<?php
 
namespace App\Service;
 
use Symfony\Contracts\HttpClient\HttpClientInterface;
 
/**
 * EkwatestApiService
 * 
 * EkwatestApiService is an API consumer targeting the 
 * ekwatest API
 */
class EkwatestApiService
{
    private $client;
    private const API_URL = "https://601025826c21e10017050013.mockapi.io/ekwatest/";
     
    /**
     * __construct
     *
     * @param  HttpClientInterface $client
     * @return void
     */
    public function __construct(HttpClientInterface $client)
    {
        $this->client = $client;
    }
    
    /**
     * Perform a get request
     *
     * @param  string $route
     * @return array
     */
    private function get($route): array
    {
        $response = $this->client->request(
            'GET',
            self::API_URL . $route
        );

        $response_array = [];
        if ($response->getStatusCode() >= 200 && $response->getStatusCode() < 300) {
            $response_array = $response->toArray();
            $request_success = true;
        }

        return [
            "success" => $request_success, 
            "status_code" => $response->getStatusCode(), 
            "response" => $response_array,
        ];
    }
     
    /**
     * getOfferList
     *
     * @return array
     */
    public function getOfferList(): array
    {
        return $this->get('offerList');
    }
    
    /**
     * getPromoCodeList
     *
     * @return array
     */
    public function getPromoCodeList(): array
    {
        return $this->get('promoCodeList');
    }
}