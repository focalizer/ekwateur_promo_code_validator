<?php
 
namespace App\Tests\Service;
 
/**
 * PromoCodeService
 * 
 * This PromoCodeService offers helpful functionnalities for Promo Code like :
 * - Validate a Promo Code
 */
class MockEkwatestData
{     
    static public function mockOfferListData()
    {
        return '[{"offerType":"GAS","offerName":"EKWAG2000","offerDescription":"Une offre incroyable","validPromoCodeList":["EKWA_WELCOME","ALL_2000"]},{"offerType":"GAS","offerName":"EKWAG3000","offerDescription":"Une offre croustillante","validPromoCodeList":["EKWA_WELCOME"]},{"offerType":"ELECTRICITY","offerName":"EKWAE2000","offerDescription":"Une offre du tonnerre","validPromoCodeList":["EKWA_WELCOME","ALL_2000","ELEC_IS_THE_NEW_GAS"]},{"offerType":"ELECTRICITY","offerName":"EKWAE3000","offerDescription":"Pile l\'offre qu\'il vous faut","validPromoCodeList":["EKWA_WELCOME","ELEC_IS_THE_NEW_GAS","BUZZ","ELEC_N_WOOD"]},{"offerType":"WOOD","offerName":"EKWAW2000","offerDescription":"Une offre du envoie du bois","validPromoCodeList":["EKWA_WELCOME","ALL_2000","WOODY","WOODY_WOODPECKER"]},{"offerType":"WOOD","offerName":"EKWAW3000","offerDescription":"Une offre souscrite = un arbre planté","validPromoCodeList":["EKWA_WELCOME","WOODY","WOODY_WOODPECKER","ELEC_N_WOOD"]}]';
    }

    static public function mockPromoCodeListData()
    {
        return '[{"code":"EKWA_WELCOME","discountValue":2,"endDate":"2019-10-04"},{"code":"ELEC_N_WOOD","discountValue":1.5,"endDate":"2022-06-20"},{"code":"ALL_2000","discountValue":2.75,"endDate":"2023-03-05"},{"code":"GAZZZZZZZZY","discountValue":2.25,"endDate":"2025-08-02"},{"code":"ELEC_IS_THE_NEW_GAS","discountValue":3.5,"endDate":"2022-04-13"},{"code":"BUZZ","discountValue":2.75,"endDate":"2022-02-02"},{"code":"WOODY","discountValue":1.75,"endDate":"2022-05-29"},{"code":"WOODY_WOODPECKER","discountValue":1.15,"endDate":"2017-04-07"}]';
    }
}