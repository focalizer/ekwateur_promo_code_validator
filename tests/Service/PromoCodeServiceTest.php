<?php

namespace App\Tests\Service;

use App\Service\PromoCodeService;
use App\Tests\Service\MockEkwatestData;
use App\Utils\ErrorCode;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpClient\MockHttpClient;
use Symfony\Component\HttpClient\Response\MockResponse;

/**
 * PromoCodeServiceTest
 * 
 * Tests for the PromoCodeService
 */
class PromoCodeServiceTest extends TestCase
{
    /**
     * testValidate
     *
     * @return void
     */
    public function testValidate(): void
    {
        $client_code_list = new MockHttpClient([
            new MockResponse(MockEkwatestData::mockPromoCodeListData())
        ]);
        $client_offer_list = new MockHttpClient([
            new MockResponse(MockEkwatestData::mockOfferListData())
        ]);

        $promo_code_service = new PromoCodeService();
        $test_result = $promo_code_service->validate("ELEC_IS_THE_NEW_GAS", $client_code_list, $client_offer_list);
        $expected_response = json_decode('{"promoCode":"ELEC_IS_THE_NEW_GAS","endDate":"2022-04-13","discountValue":3.5,"compatibleOfferList":[{"name":"EKWAE2000","type":"ELECTRICITY"},{"name":"EKWAE3000","type":"ELECTRICITY"}]}', true);

        $this->assertEquals([ true, $expected_response ], $test_result);
    }

    /**
     * testValidateFailDoesntexist
     *
     * @return void
     */
    public function testValidateFailDoesntexist(): void
    {
        $client_code_list = new MockHttpClient([
            new MockResponse(MockEkwatestData::mockPromoCodeListData())
        ]);
        $client_offer_list = new MockHttpClient([
            new MockResponse(MockEkwatestData::mockOfferListData())
        ]);

        $promo_code_service = new PromoCodeService();
        $test_result = $promo_code_service->validate("TOTO", $client_code_list, $client_offer_list);

        $this->assertEquals([ false, ErrorCode::PROMO_CODE_DOESNT_EXIST ], $test_result);
    }

    /**
     * testValidateFailNotInOffer
     *
     * @return void
     */
    public function testValidateFailNotInOffer(): void
    {
        $client_code_list = new MockHttpClient([
            new MockResponse(MockEkwatestData::mockPromoCodeListData())
        ]);
        $client_offer_list = new MockHttpClient([
            new MockResponse(MockEkwatestData::mockOfferListData())
        ]);

        $promo_code_service = new PromoCodeService();
        $test_result = $promo_code_service->validate("GAZZZZZZZZY", $client_code_list, $client_offer_list);

        $this->assertEquals([ false, ErrorCode::PROMO_CODE_NOT_IN_OFFER ], $test_result);
    }

    /**
     * testValidateFailHasExpired
     *
     * @return void
     */
    public function testValidateFailHasExpired(): void
    {
        $client_code_list = new MockHttpClient([
            new MockResponse(MockEkwatestData::mockPromoCodeListData())
        ]);
        $client_offer_list = new MockHttpClient([
            new MockResponse(MockEkwatestData::mockOfferListData())
        ]);

        $promo_code_service = new PromoCodeService();
        $test_result = $promo_code_service->validate("WOODY_WOODPECKER", $client_code_list, $client_offer_list);

        $this->assertEquals([ false, ErrorCode::PROMO_CODE_HAS_EXPIRED ], $test_result);
    }
}
